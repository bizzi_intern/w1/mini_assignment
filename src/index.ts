import fs from "fs";
const file = fs.readFileSync("src/data/email.txt", "utf-8");

// Remove new lines for better data searching
const modifiedFile = file.replace(/\r\n/g, " ");

const maSoThueRegex = /Mã số thuế\s*([^\s]*)/;
const mauSoRegex = /Mẫu số:\s*([^\s]*)/;
const kyHieuRegex = /Ký hiệu:\s*([^\s]*)/;
const soHoaDonRegex = /Số hóa đơn:\s*([^\s]*)/;
const maTraCuuRegex = /Mã tra cứu:\s*([^\s]*)/;
const linkRegex = /\[(http.*?)\]/g;

const maSoThue = maSoThueRegex.exec(modifiedFile);
const mauSo = mauSoRegex.exec(modifiedFile);
const kyHieu = kyHieuRegex.exec(modifiedFile);
const soHoaDon = soHoaDonRegex.exec(modifiedFile);
const maTraCuu = maTraCuuRegex.exec(modifiedFile);

let link;
let links = [];
do {
  link = linkRegex.exec(modifiedFile);
  if (link) {
    links.push(link?.["1"]);
  }
} while (link);

console.log("Mã số thuế:", maSoThue?.["1"]);
console.log("Mẫu số:", mauSo?.["1"]);
console.log("Ký hiệu:", kyHieu?.["1"]);
console.log("Số hóa đơn:", soHoaDon?.["1"]);
console.log("Mã tra cứu:", maTraCuu?.["1"]);
console.log("Các đường links:", links);
