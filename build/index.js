"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs_1 = __importDefault(require("fs"));
const file = fs_1.default.readFileSync("src/data/email.txt", "utf-8");
// Remove new lines for better data searching
const modifiedFile = file.replace(/\r\n/g, " ");
const maSoThueRegex = /Mã số thuế\s*([^\s]*)/;
const mauSoRegex = /Mẫu số:\s*([^\s]*)/;
const kyHieuRegex = /Ký hiệu:\s*([^\s]*)/;
const soHoaDonRegex = /Số hóa đơn:\s*([^\s]*)/;
const maTraCuuRegex = /Mã tra cứu:\s*([^\s]*)/;
const linkRegex = /\[(http.*?)\]/g;
const maSoThue = maSoThueRegex.exec(modifiedFile);
const mauSo = mauSoRegex.exec(modifiedFile);
const kyHieu = kyHieuRegex.exec(modifiedFile);
const soHoaDon = soHoaDonRegex.exec(modifiedFile);
const maTraCuu = maTraCuuRegex.exec(modifiedFile);
let link;
let links = [];
do {
    link = linkRegex.exec(modifiedFile);
    if (link) {
        links.push(link === null || link === void 0 ? void 0 : link["1"]);
    }
} while (link);
console.log("Mã số thuế:", maSoThue === null || maSoThue === void 0 ? void 0 : maSoThue["1"]);
console.log("Mẫu số:", mauSo === null || mauSo === void 0 ? void 0 : mauSo["1"]);
console.log("Ký hiệu:", kyHieu === null || kyHieu === void 0 ? void 0 : kyHieu["1"]);
console.log("Số hóa đơn:", soHoaDon === null || soHoaDon === void 0 ? void 0 : soHoaDon["1"]);
console.log("Mã tra cứu:", maTraCuu === null || maTraCuu === void 0 ? void 0 : maTraCuu["1"]);
console.log("Các đường links:", links);
